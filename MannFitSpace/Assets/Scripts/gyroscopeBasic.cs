﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gyroscopeBasic : MonoBehaviour {

    
    int absementScore;
    int initialWaitTime= 10;
    float timeCounter=0;
    bool timeSet=false;
    Quaternion initialBoardRotation;

    public GameObject Text;

	// Use this for initialization
	void Start () {
        Input.gyro.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

        //switch the -z and y 
        transform.localRotation = new Quaternion(Input.gyro.attitude.x,-1*Input.gyro.attitude.z,Input.gyro.attitude.y,Input.gyro.attitude.w);
        
        timeCounter=timeCounter+Time.deltaTime;


        // initial offset
        if (!timeSet)
        {
            timeCounter += Time.deltaTime;
            if (timeCounter >= initialWaitTime)
            {
                initialBoardRotation = transform.localRotation;
                // print("initialBoardRotation " + initialBoardRotation.eulerAngles.ToString());
                timeSet = true;
            }
        }
        else
        {

            //scoring

            float score =Mathf.Abs( transform.localRotation.eulerAngles.magnitude - initialBoardRotation.eulerAngles.magnitude);
            print("score " + score);
            absementScore += Mathf.Abs(Mathf.RoundToInt(score));
            
            //display
            Text.GetComponent<Text>().text = "Absement Score" +absementScore.ToString();
        }
    }
}
